SOAT (https://postech.fiap.com.br/plataforma)
Software Architecture (https://postech.fiap.com.br/plataforma)
	DDD
	 Embora o DDD seja importante e precise estar na caixa de ferramentas do eng./ arquiteto de software, é preciso ter parcimônia, sabedoria e senso crítico para saber quando aplicar os conceitos, de forma parcial ou integral.
	 
	 Cuidado com o over engineering & big design upfront
	 
	 Over engineering é um termo usado para descrever uma situação em que um sistema de software é projetado ou implementado de forma excessivamente complexa, com mais recursos, funcionalidades ou componentes do que o necessário para atender às necessidades do negócio. Isso pode levar a problemas de desempenho, manutenção e escalabilidade, além de aumentar os custos e o tempo de desenvolvimento.
	 
	 Big Design Upfront (BDUF) é um termo relacionado que se refere a uma abordagem de desenvolvimento de software que enfatiza a realização de um design detalhado e completo antes de começar a escrever o código. Essa abordagem pressupõe que é possível antecipar todas as necessidades e requisitos do sistema no início do projeto, o que nem sempre é possível na prática. BDUF pode levar a um excesso de documentação e análise, tornando o processo de desenvolvimento mais lento e rígido.
	 
	 Ambos os termos são usados para descrever práticas de desenvolvimento de software que podem levar a problemas de ineficiência e inflexibilidade. O over engineering pode ocorrer quando os desenvolvedores tentam criar um sistema excessivamente complexo para atender às necessidades do negócio, enquanto o BDUF pode levar a um excesso de análise e documentação, que pode não refletir completamente a realidade do projeto. Em ambos os casos, é importante encontrar um equilíbrio entre as necessidades do negócio e as capacidades do sistema, evitando a criação de sistemas desnecessariamente complexos ou inflexíveis.
		Aula 1 - Introdução ao DDD
			Conceito
				DDD (Domain-Driven Design) é uma abordagem de design de software que tem como objetivo desenvolver sistemas baseados em um entendimento profundo do domínio de negócio, ou seja, na lógica e nas regras que regem a operação de uma organização. Essa abordagem busca alinhar o código do software com a linguagem e o conhecimento do domínio de negócio, facilitando a comunicação entre desenvolvedores e especialistas no negócio.
					Embora não seja uma metodologia de projeto, ajuda a criar uma cultura de comunicação e documentação
					O conhecimento do negócio nunca é centralizado, o conhecimento está espalhado por diversos membros do time
				Design Estratégico
					Porque queremos fazer?
					O que queremos fazer?
					Como fazer?
					Onde queremos chegar?
				Domínios & Subdomínios
				 Em DDD, o Domínio de Negócio é o conjunto de conceitos, regras e processos que representam a lógica de negócio de uma organização. Para lidar com a complexidade do domínio de negócio, é comum dividir o domínio em subdomínios, que representam áreas distintas do negócio.
				 
				 Os Subdomínios Principais são as partes do domínio que representam a lógica de negócio central da organização, ou seja, as áreas de maior valor para o negócio. Eles geralmente incluem as funcionalidades e regras de negócio mais importantes.
				 
				 Os Subdomínios Genéricos são as partes do domínio que são comuns a muitas organizações. Eles representam as funcionalidades e regras de negócio que não são exclusivas da organização, mas que ainda são importantes para o negócio. Exemplos de subdomínios genéricos incluem autenticação, segurança, gerenciamento de usuários, entre outros.
				 
				 Os Subdomínios de Suporte são as partes do domínio que não estão diretamente relacionadas à lógica de negócio principal da organização, mas que ainda são importantes para o suporte ao negócio. Eles geralmente incluem funcionalidades como gerenciamento de infraestrutura, monitoramento, suporte ao usuário, entre outros.
				 
				 A divisão do domínio em subdomínios ajuda a reduzir a complexidade do sistema, tornando mais fácil entender e manter o código. Cada subdomínio pode ter sua própria equipe de desenvolvimento responsável por sua implementação, e as interações entre os subdomínios podem ser gerenciadas por meio de context mapping, garantindo a integração e consistência do sistema como um todo.
					Domínio: Conjunto de regras, processos, terminologias e conceitos que são relevantes a um determinado negócio ou organização. Conhecimento e expertise específicos.
					Subdomínios Principais: negócios que diferenciam dos outros mercados. Áreas ou pares distintas do domínio que são estratégicas, centrais e essenciais para a maior parte do valor do negócio.
					O que faz o negócio especial? Qual o diferencial?
					Subdomínios Genéricos: são partes do domínio que são comuns às organizações. Embora importantes, não são o core do negócio e não apresentam vantagem para o negócio
					Subdomínio de Suporte: são partes do domínio que nano estão diretamente ligadas à lógica do negócio. Tem o objetivo de complementar o domínio principal
					Fluxograma para identificação de domínio
					
				Domain Expert
					Pessoa que possui o conhecimento profundo do negócio, domínios e subdomínios
					É capaz de descrever todos os processos e procedimentos, regulações, regras, problemas comuns, etc.
					Tem objetivo de suportar o time de desenvolvimento a garantir que o software seja construído de acordo com as necessidades do negócio e expectativa dos usuários
			Palavras chaves
				DDD
				Modelagem de Negócios: é uma técnica que ajuda a compreender e documentar os processos, atividades e estruturas de uma organização. É uma forma de representar a lógica de negócio de uma empresa por meio de diagramas e outros tipos de modelos visuais.
				Event Storming: é uma técnica de modelagem de processos de negócio que se concentra em eventos, ou seja, nas ações que ocorrem no sistema ou no domínio de negócio. Essa técnica é usada para explorar e entender um processo de negócio em detalhes, identificando as etapas, as ações, as decisões e as regras envolvidas. Durante um Event Storming, os participantes colaboram em uma sessão de workshop intensiva, com o objetivo de criar um modelo visual e compartilhado do processo.
				Em conjunto, essas técnicas podem ser usadas para criar sistemas de software mais alinhados com o negócio e com as necessidades dos usuários, e para documentar e compreender melhor os processos de negócio de uma organização. Ao utilizar o DDD em conjunto com a Modelagem de Negócios e o Event Storming, é possível ter uma visão mais clara e completa do domínio de negócio, facilitando a identificação de oportunidades de melhoria e a implementação de soluções mais efetivas.
			Hands On
				Introdução DDD I
					DDD é sobre criar soluções e não software
					Resolver problemas de forma estruturada
					Pensar antes de escrever códigos
					Escrever códigos para os outros entenderem
					Escrever soluções para o negócio
					Desafios de Projetos
						Falta de clareza nos objetivos
						Scope Creep: escopo sempre muda
						Expectativas irreais
						Recursos Limitados
						Falha na comunicação
						Atraso nas entregas
						Falta de transparência
				Introdução DDD II
					Domínio é o negócio em si, o motivo da existência. O resto é acessórios. O domínio vive dentro de um contexto 
					Subdomínio Principal: o motivo de existência; possui diferencial competitivo; possui lógica complexa; coração do negócio
					Subdomínio Genérico: Não possui diferencial de negócio; é comum a todas as organizações;
						
					Subdomínio de Suporte: apoia o negócio da empresa, complementa o que o domínio principal faz
					O core sempre deve ser priorizado em detrimento do subdomínos de suporte/genérico
				Introdução DDD III
					Domain expert deve contar a história; pode possuir vários domain experts por subdomínios
					Subdomínio são definidos em função do contexto. Tudo depende do risco vs recompensa
					Documentar as histórias obtidas pelo Domain Expert é um desafio.
					Técnicas de documentação devem ser utilizadas para melhor aplicação do DDD
		Aula 2 - Domain Storytelling
			Conceito
				Prática para ajudar na criação de histórias claras, com o objetivo de entendermos melhor o negócio e a jornada do cliente
				Métodos e técnicas para entender melhor os domínios e subdomínios
				Objetivos
					Entender o domínio
					Estabelecer linguagem comum entre domain experts e IT experts
					Evitar mal entendidos
					Esclarecer os requisitos 
					Implementar a solução de forma correta, atendendo as expectativas do cliente
					Estruturar o software
					Desenhar os processos de negócios que são viáveis
				Linguagem Pictográfica
					Os pictogramas são imagens, que representam um conceito ou objetos; utilizados para ilustrar um determinado item
					Facilita na criação de uma linguagem coesa
				Atores
					Pode ser uma pessoa, grupo, sistema, objeto
					A narrativa é escrita na perspectiva do ator
				Objetos de Trabalho
					Objetos físicos, digitais, planilha, documentos, iterações por email, telefone, etc
				Atividades
					Ações dos atores com os objetos de trabalho
					São representadas por setas
					Indicam o que acontece no fluxo de atividade
					Não existe condicionais (if-else). Os fluxos alternativos devem ser tratados por novas histórias
					Não utiliza retornos (loopback) no mesmo fluxo
				Números Sequenciais
					As histórias devem possuir um número sequencial lógico
					Para excuções paralelas, as histórias devem ser numeradas igualmente
				Anotações
					Utilizadas para enriquecer a as histórias com anotações sobre limitações, triggers, pontos de atenção, ações, eventos, etc
				Grupos
					São representação de partes das histórias
					Podem representar ações repetidas, subdomínios, etc
				Cores
					Diferentes cores podem ser utilizadas para destacar um determinado fluxo ou atividades
				Cenários
					Variações nos fluxos das histórias
				Escopo
					Delimitação da história, com início, meio e fim
				Realidade vs Desejo
					AS IS: realidade atual
					TO BE: realidade nova ou futura
				Equipe de Trabalho
					Domain Experts: quando houver a necessidade de contar a história
					Ouvintes: pessoas envolvidas que necessitam de conhecer a história; normalmente o time de desenvolvimento
					Moderador: conduz as conversas e mantém os envolvidos engajados atingir o objetivo
					Modelador: cria a história em linguagem pictográfica e realiza as devidas anotações
				Técnica colaborativa, onde vários perfis, diferentes pessoas colaboram para deixar a história fluida e coesa
			Hands On
				Domain Storytelling I
					egon.io (http://egon.io) é utilizada como ferramenta de pictograma
					Tem o objetivo de exibir o fluxo de forma gráfica e estuturada
					As nuâncias do processo deve ser documentada em forma de anotações
					A partir das histórias de usuários contadas pelo domain expert, entende-se o que está acontecendo em cada contexto, e de forma visual, deixa claro os fluxos do domínio
				Domain Storytelling II
					Indicado criar um documento de storytelling por fluxo
					A separação pode ser feita por cenários
					A linguagem ubíqua é importante para alinhar o time técnico com o time de negócio
				Domain Storytelling III
					Para fluxos alternativos (if-else), um novo cenário deve ser criado
					Deve-se definir de forma objetiva e clara o escopo
					É importante definir já no começo do domain storytelling se o que está sendo mapeado é o AS-IS ou o TO-BE
					Segundo Alistair Cockburn (Escrevendo Casos de Uso Efetivos), os 05 níveis de detalhamento
						Nuvem
						Pipa
						Mar
						Peixe
						Ostra
					No detalhamento do caso de uso, é indicado ir até o nível do “mar”, ou seja, a visão não pode ser tão macro, nem tem micro
					AS IS vs TO BE: atentar para não automatizar o processo atual por si só. O TO BE pode/deve otimizar o processo
					Domínio Puro & Digitalizado
						Domínio Puro: é mostrada apenas a iterações entre atores, sem destacar nenhuma tecnologia.
						Domínio Digitalizado: além do fluxo e iterações entre atores, a tecnologia é ilustrada
					Equipe de Trabalho
						Domain Experts, time de desenvolvimento, arquiteto, demais envolvidos
						De preferência as reuniões devem ser em salas isoladas, sem celulares; todos com o foco na entrega do história
		Aula 3 -  Descoberta e formação de conhecimento
			Conceito
				Linguagem Ubíqua: linguagem comum e compartilhada entre todos os membros da equipe de desenvolvimento e usuários. O objetivo é garantir uma compreensão clara e precisa dos termos utilizados, evitando mal-entendidos e ambiguidades. Essa linguagem é estabelecida por meio de um processo colaborativo de definição e documentação dos termos técnicos e conceitos utilizados na equipe.
					Termos Ambíguos: mesmo termo com vários significados, dependendo do subdomínio/contexto
					Termos Sinônimos: mesmo significado ou muito similar
					Deve-se quebrar os termos com definições únicas, evitando assim problemas de entendimento
				Bounded Contexts: técnica utilizada em para definir e gerenciar os limites e fronteiras dos diferentes subdomínios de um sistema. É a maneira de separar e organizar a lógica de negócio em unidades distintas e independentes, permitindo que cada subdomínio evolua de forma autônoma.
				
				Cada Contexto Delimitado representa um conjunto de conceitos e regras de negócio relacionados, que juntos formam um subdomínio específico. Esses contextos podem ser definidos com base em critérios como responsabilidades, modelos de dados, operações de negócio, limites organizacionais ou até mesmo em função das tecnologias utilizadas.
				Modelagem de Domínio: documentação de um processo de negócio ao qual estamos resolvendo um problema; Cabe observar que é importante a definição do domínio mapeado: AS-IS vs TO-BE
			Hands On
				Descoberta e Formação de Conhecimento I
					O ordem discriminada em domain storytelling demonstra a ordem das ações e dos eventos
					A linguagem ubíqua é importante para determinar e compreender os contextos (subdomínios)
					Distintos atores podem ter nomes diferentes de acordo com o contexto inserido
					A modelagem do domínio é a abstração do processo, como as coisas se relacionam entre si
					Ferramentas
						Wiki: descrição do projeto, dicionários da linguagem ubíqua, etc
						Repositórios: versionamento de documentos 
						Jira/Trello/etc: gestão de projetos, controlando a evolução, features, atividades, escopo, prazo,  etc
				Descoberta e Formação de Conhecimento II
					Contextos separados em pacotes separados
					A linguagem ubíqua deve ser utilizada na arquitetura do sistema (pacotes, classes, módulos, etc)
					Contextos delimitados: provenientes das semelhanças entre elementos, principalmente da linguagem ubíqua que foi mapeada; são limites impostos em um modelo de domínio
						Os tamanho dos contextos devem ser o menor possível. Dessa forma é possível controlar menor as funções, objetos, métodos; provê maior coesão entre os pontos compartilhados;
					O número de contextos podem mostrar o tamanho do time
					Um time pode trabalhar em diversos contextos, porém, um contexto deve ser trabalhado por apenas um time
			Palavras Chaves
				Modelagem de Negócio
				Linguagem Ubíqua
		Aula 4 - Trabalhando com contextos delimitados
			Hands On
				Trabalhando com contextos delimitados I
					Modelos de Cooperação
						Parceria: comunicação constante entre times, flúida e colaborativos; os times se notificam sobre mudanças que possam impactar outros contextos;
						Kernel Compartilhado: parte do modelo é comum a todos os contextos envolvidos; a comunicação deve ser efetiva, pois este modelo viola as fronteiras de um contexto delimitado; necessita do compartilhamento do mesmo código entre os times envolvidos; geralmente sistemas legados (monólitos) necessitam de kernel compartilhado
						Cliente-Fornecedor: Fornecedor (upstream): provê um serviço; Cliente (downwstream) consome um serviço; os times podem desenvolver os contextos de forma separada, porém, com dependências de integração
						Conformista: quando existe dependência com padrões determinados (upstream) e o cliente (downstream) apenas se conforma com os contratos estabelecidos
						Anti Corruption Layer: camada adicional com o objetivo de realizar abstrações    e/ou adaptações entre upstream & downstream
				Trabalhando com contextos delimitados II
					Modelos de Cooperação
						Open-Host Service: quando fornecedor (upstream) cria uma camada de abstração no seu serviço para adaptar as necessidades do cliente (downstream); publicação de API
						Linguagem Publicada: o fornecedor upstream, além de ter uma camada de abstração para o downstream (OHS), ele também permite diversas versões personalizadas de linguagem para cada cliente; publicação de API customizada para o cliente
						Caminhos Separados: cenário onde a integração de contexto é inviável ou de alto custo; os times não se integram; a implementação é duplicada
						Grande Bola de Lama (BBoM): contextos muito grandes e/ou limitados e inconsistentes
						ACL vs PL: ACL é implementado do downstream, enquanto a PL é implementado no upstream
					Mapa de Contexto
						Materialização visual dos contextos, destacando suas iterações
			Palavras Chaves
				Contextos Delimitados
				Cliente-Fornecedor
				Mapa de Contexto
		Aula 5 - Implementando arquitetura e lógica
			Conceito
				Design Tático
					Se trata de como fazer, como materializar, por meio de padrões, a solução de software
					Quais tecnologias, de que forma atender os requisitos não funcionais
					De que forma integrar
			Hands On
				Implementando arquitetura e lógica I
					Definir a arquitetura, como cada subdomínio será implementado, onde ficarão as lógicas
					Arquitetura no contexto do DDD
						Canada de Interface de Usuário: contém GUIs, CLIs e APIs
						Camada de Aplicação: Mediação entre a camada de usuário e domínio
						Camada de Domínio: contém o conceito e as regras do negócio
						Camada de Infraestrutura: camada de suporte para a camada superior; acesso a dados; barramentos; integrações, etc
				Implementando arquitetura e lógica II
					Blocos Fundamentais
						Objetos de Valor
							Não possui identificadores
							Medem, quantificam ou descrevem os objetos
							São imutáveis
							São comparados pela igualdade de valores
							Se um atributo muda, cria-se um objeto completamente novo
				Implementando arquitetura e lógica III
					Blocos Fundamentais
						Entidades
							Identificado através de um ID
							São mutáveis
							Pode possuir objetos de valores pra definir seu estado
						Agregados
							Conjunto de Entidades e Objetos de Valor que se relacionam entre si
							Somente o agregado pode alterar o seu estado
							Contém as entidades
							O estado é alterado por comandos/interfaces
							Contém uma classe principal que comanda a iteração com  outras entidades
						Serviços de Domínio
							Relaciona com várias entidades e agregados, realizando cálculos, executando regras, etc 
		Aula 6 - Event Storming
			Conceito
				Event Storming: técnica de modelagem visual e colaborativa; durante a sessão, os participantes representam os eventos e reações do domínio de negócios com cartões e post-its, organizados em ordem cronológica e em diferentes camadas do sistema. O resultado é um modelo interativo e visual do domínio, que ajuda a equipe de desenvolvimento a obter uma compreensão compartilhada e alinhada do domínio de negócios, reduzindo a complexidade e os riscos do projeto.
				Eventos Pivotais: são eventos significativos no domínio de negócios que desencadeiam mudanças em outros eventos ou regras de negócios relacionadas; ajudam a equipe de desenvolvimento a compreender melhor o domínio e a definir os limites dos subdomínios; ajuda a equipe de desenvolvimento a trabalhar de forma mais eficiente e colaborativa, identificando as principais áreas de risco e oportunidade no projeto.
			Envolvidos
				Domain Experts 
				Agregados & Time de desenvolvimento
				Facilitador/Mediador
			Hands On
				Event Storming I
					Elementos gráficos
					1. Brainstorming
						Na fase de brainstorming, os eventos são descobertos e não são sequenciais
						Devemos descrever os eventos no passado
					2. Refinamento
						Refinar e remover os eventos que estão duplicados
						Revisar 
					3. Ordenar e relacionar
						Ordenar os eventos
						Relacionar os eventos
						Mapear fluxos alternativos
					4. Mapear pontos de atenção
						Levanta e refinar os pontos de atenção
					5. Identificar os eventos pivotais
				Event Storming II
					Comandos: são descritos no imperativo e deve destacar qual ator ou política (ou sistema) que disparou o evento
					Modelo de Leitura: representa a interface ou meio que o ator vai utilizar para tomar uma ação; sempre precedem um comando
					Sistema Externo: integrações diversas
					Agregados: identifica o objeto principal de cada passo; os eventos pivotais pode dar dica de quais são os grupos e eventos, e assim, identificar os agregados
					Contextos Delimitados: combinação de um ou mais agregados; geralmente definidos pela linguagem ubíqua  
	Links Externos
		Geral
			Domain Storytelling (https://domainstorytelling.org)
		Ferramentas
			Miro (https://miro.com): ferramenta colaborativa de quadro branco virtual, que permite a criação de diagramas, fluxogramas e outras visualizações em equipe.
			Egon (https://egon.io): plataforma de gestão de projetos e tarefas, que permite a organização e acompanhamento de atividades em equipe, com recursos como kanban e calendários compartilhados.
			Notion (https://notion.so): ferramenta de organização e gestão de tarefas, projetos e informações, que permite a criação de notas, bancos de dados, listas de tarefas e outros recursos em uma interface unificada e customizável.
			Figma (https://www.figma.com): ferramenta de design e prototipagem colaborativa baseada na nuvem, utilizada por equipes de design de software para criar interfaces de usuário, wireframes e protótipos.
		Material Complementar
			DDD do jeito certo (https://www.youtube.com/playlist?list=PLkpjQs-GfEMN8CHp7tIQqg6JFowrIX9ve)